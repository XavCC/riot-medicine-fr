\chapter{Oral and Dental Injuries}

\epigraph{I die like a true blue rebel. Don't waste any time in mourning. Organize.}
{Joe Hill}

\noindent
Those of us who have played full-contact sports know the importance of protecting our teeth.
Mouth guards are often mandatory, and even still many people have had teeth knocked out or made loose after a particularly savage hit.
At demonstrations, few if any participants wear such protection, but despite this, injuries to the mouth beyond a split lip or a bit cheek are uncommon relative to other injuries.
As violence and the physicality of demonstrations increase by whatever means, or as our luck runs out, someone will take a hit that damages their mouth and teeth, and a bit of knowledge can salvage a damaged or lost tooth.

\section*{Physiology}

\index{oral cavity|(}

The technical term for the mouth is the oral cavity.
It is (approximately) bounded by the lips, pharynx, hard palate, and the fascial tissue under our tongue.
Structures within the mouth include teeth, the tongue, gingiva\index{gingiva} (gums), and the soft oral mucosa like the tissue on the top of our mouth and cheeks.
The gingiva is the mucosal tissues that covers the maxilla (bone of the upper mouth) and mandible (jawbone) and surrounds the exposed parts of the teeth.

\index{oral cavity|)}

\begin{figure}[htbp]
\centering
\caption{Mouth Anatomy\supercite{cuibonobo}}
\label{fig:mouth_anatomy}
\includesvg[height=11cm, width=\textwidth, keepaspectratio]{mouth-anatomy}
\end{figure}

\index{teeth|(}

Humans have two sets of natural teeth in their lifetimes.
Primary teeth (deciduous teeth, baby teeth) develop during in the human embryo and erupt during infancy.
They fall out (exfoliate) starting around age 6 until all have been replaced with permanent teeth (adult teeth) by around age 12.

\begin{figure}[tbhp]
\centering
\caption{Types of Teeth\supercite{bizhan}}
\includesvg[height=2.5cm, keepaspectratio]{teeth-types}

\vspace{0.5cm}
{\centering From left to right: incisor, canine, premolar, and molar.}
\end{figure}

When discussing teeth, the mouth is divided into left/right and upper/lower quadrants.
The upper and lower sets are respectively named the maxillary and mandibular teeth, named after the alveolar bones they are attached to.
There are typically\footnotemark[1] 32 teeth in the set of permanent teeth, and there are 4 types of teeth, each evenly divide among the quadrants.
From front to back, there are 8 incisors, 4 canines, 8 premolars, and 12 molars.

\footnotetext[1]{
This variation is because humans may have zero, one, or multiple wisdom teeth per quadrant of the mouth, although one per quadrant is considered typical.
}

The tooth is divided into two regions.
The crown is the part of the tooth that is exposed and is visible above the gingiva\index{gingiva}.
The crown is covered in enamel, the hardest substance in the human body.
Enamel is natural white, but may become stained yellow-brownish over time.
Beneath the crown is dentin, a system of microtubes consisting of minerals and structures that are less hard than enamel.
Dentin has a creamy yellow color relative to the white enamel.
Within the dentin is the pulp\footnotemark[2] of the tooth which consisting of connective tissue, blood vessels, and nerves that enter it from the root of the tooth.
The pulp has a redish-pink color.
The root is the part of the tooth below the gingiva that extends into the alveolar bone.
It is made of dentin with a covering of a bone-like structure called cementum that is softer than both enamel and dentin.
Cementum's primary role is the medium by which which the periodontal ligaments attach the tooth to the alveolar bone.

\footnotetext[2]{
The pulp is colloquially called ``the nerve'' of the tooth, although this is reductive misnomer.
}

\begin{figure}[tbhp]
\centering
\caption{Tooth Anatomy\supercite{cuibonobo}}
\includesvg[height=12cm, width=\textwidth, keepaspectratio]{tooth-anatomy}
\end{figure}

Enamel protects the dentin of the tooth from the oral environment.
Loss of enamel either through chipping, cavities, or other degradation can expose the dentin of the tooth.
Cavities that extend to the dentin can rapidly extend to the pulp and cause the loss of a tooth, so it is important that the dentin is protected.
In the context of riot medicine, this means that patients with dental injuries should be directed to dentists for treatment to help prevent subsequent tooth loss.

\index{teeth|)}

\section*{General Treatment}

All oral and dental injuries are head injuries, and they should be treated as such.
Depending on the mechanism of injury (hit vs. jostled) and in particular the force of the hit, medics should be highly suspicious of the presence of a concussion or other head and neck injuries.
When the patient presents with oral or dental injuries, consider first performing a basic check for traumatic brain injuries as discussed in \fullref{ch:brain_and_spinal_cord_injuries}.
Continue monitoring the patient for developing symptoms of a \acrshort{TBI} while you treat their primary injury.

Additionally, the use of instant cold compresses can help reduce pain and swelling in the face and jaw, therefore they are recommended.

\section*{Oral Injuries}
\index{trauma!oral|(}

Oral injuries are to the mouth that exclude dental injuries or fractures to the maxilla or mandible.

\subsection*{Oral Cavity Mucosal Lacerations}

\index{braces!orthodontic| see {orthodontic braces}}

Oral cavity mucosal lacerations are lacerations to the soft tissue inside the mouth.
These are likely the most common oral injury at actions as even a minor hit to the head including being jostled in a crowd can cause an individual to bite their cheek, and this may be exacerbated by the presence of orthodontic braces\index{orthodontic braces} which can shred the inside of the cheeks.
However, since these are often minor and are an injury we're all familiar with, most patients likely do not seek treatment and will simply spit blood until the issue resolves itself.\footnotemark[3]

\footnotetext[3]{
Pride or the ``coolness'' of casually spitting blood (toxic masculinity lol) also seem to be significant factors in individuals not seeking treatment after being hit.
}

\subsubsection*{Treatment}

Bleeding should be staunched with folded or rolled up gauze compresses.
Place the compresses inside the patient's mouth and have them apply pressure from the outside using their hand to press their cheek against the gauze.
Packing their mouth can staunch bleeding form the mucosa under the tongue.
Because of the vascularity of oral mucosa, injuries heal quickly on their own without intervention.
However, injuries to the inner rear part of the cheek can damage the parotid duct, and injuries beneath the tongue can damage the submandibular duct.
Injuries to these salivary ducts may require surgical repair, and this is not something a medic can determine.
Patients with lacerations beyond minor abrasions should be sent to advanced medical care for evaluation.

\subsection*{Lip Lacerations}

A bloody lip that arises from abrasion does not require significant treatment beyond controlling bleeding and cleaning the wound.
Short lacerations to the lip ($<$\SI{1}{\cm}) that only affect the oral mucosa may additionally not require sutures.
Other lacerations, in particular a through-and-though laceration (``split lip''), require sutures and possible antibiotics from advanced medical care.
Not suturing the wound can have significant cosmetic as well as functional impact for the patient.
Because of how much the mouth moves, it is highly unlikely even minimally effective closure of any lip wounds can be facilitated by medics using wound closure strips.
Clean and irrigate the wound, stop the bleeding, and direct the patient to advanced medical care.

\begin{figure}[tbhp]
\centering
\caption{Lip Laceration\supercite{bizhan}}
\includesvg[height=5cm, keepaspectratio]{lip-laceration}
\end{figure}

\subsection*{Frenulum Lacerations}

Lacerations to the three frenula of the mouth as identified in \autoref{fig:mouth_anatomy} will heal on their own without requiring sutures.\supercite[p.1608]{tintinallis}
These injuries can be quite painful, in particular a laceration of the maxillary labial \gls{frenulum}, and as such the administration of analgesics is recommended.
These injuries may be treated with sutures, so it is still appropriate to direct a patient to advanced medical care if they want additional treatment.

\subsection*{Tongue Lacerations}

If the laceration is on the top of the tongue, short ($<$\SI{1}{\cm}), linear, and does not gape, it does not require sutures to heal.\supercite[p.1608]{tintinallis}
However, delayed swelling can compromise the airway, and as such anything beyond minor bit injuries should be sent to advanced medical care for better evaluation.

\section*{Dental Injuries}
\index{trauma!dental|(}

Dental injuries are injuries to the teeth and alveolar bone.
In cases where advanced medical care is indicated, if a dentist is not immediately available, initial treatment can be done by hospital's emergency department.
This can help prevent tooth loss and should be recommended to patients.

\subsection*{Cracked Tooth}

A cracked tooth is a tooth with an incomplete fracture that may extend into the pulp.
This can be painful on its own, and pain may be exacerbated by cold and sugary stimuli.
Pain generally ceases when patient's jaw is unclenched or they are not chewing.
Field treatment is the administration of analgesics.
Direct the patient to avoid chewing, clenching their jaw, or getting into situations that may lead to additional injury.
Definitive treatment requires a dentist, and the patient should see one as soon as possible, ideally on the current or following day.

\subsection*{Dental Fracture}

A dental fracture (``chipped tooth'') is a fracture of the tooth that leads to loss of structure.

In cases where a fracture causes a fragment to be removed from the tooth, if the fragment is recovered and kept moist, it can bonded back to the tooth by a dentist.
If the lost part of the tooth cannot be found and the patient has lacerations in their mouth, the patient should be directed to advanced medical care where a radiologist can ensure that the fragment is not trapped in the wound as it heals.

There are different ways to classify dental fractures, but for riot medics, there is one classification method with three types that can use to guide treatment.

\begin{figure}[htbp]
\caption{Dental Fracture Types\supercite{cuibonobo}}
\centering
\includesvg[width=\textwidth, height=10cm, keepaspectratio]{tooth-fracture}
\end{figure}

\begin{figure}[htbp]
\caption{Dental Fracture Appearances\supercite{bizhan}}
\centering
\includesvg[width=\textwidth, height=6cm, keepaspectratio]{dental-fracture-appearances}
\end{figure}

\triplesubsection{Enamel fractures}
This type of fracture only affects the enamel of the crown of the tooth.
They require no emergency treatment, but the tooth fragment can bonded back to the tooth by a dentist.

\triplesubsection{Enamel-dentin fractures}
This type of fracture affects both the enamel and dentin of the crown of the tooth.
The patient will have sensitivity to hot and cold stimuli including air moving over their tooth from breathing.
Delaying treatment by even 24 hours can lead to necrosis of the pulp and loss of the tooth.

\triplesubsection{Enamel-dentin-pulp fractures}
This type of fracture penetrates down to the pulp of the tooth.
The pinkish pulp is easily identifiable after irrigating the tooth and wiping away blood.
Surgical treatment is needed, and the patient should be sent to advanced medical care.

\subsection*{Luxation Injuries}

A luxation injury (``loose tooth'') is an injury that causes loosening of the tooth from the connective apparatus.
This type of injury is quite common and accounts for 50\% of injuries to the teeth.\supercite[p.1605]{tintinallis}
These injuries may present with obvious mobility of the tooth (``wiggling'') or displacement laterally or vertically into the alveolar socket.
There may be no mobility but sensitivity to percussion.
Treatment is to splint the tooth in place with rolled or folded gauze compresses and to direct the patient to advanced medical care.

\subsection*{Avulsions}
\index{avulsions!tooth|(}

A tooth may become avulsed (``knocked out''), but it may be replanted at the scene.
Prompt replantation increases the chances of the tooth's long-term survival.

\subsubsection*{Treatment}

Treatment is to replant the tooth if possible and transport the patient to advanced medical care.

\triplesubsection{Clean the tooth}
Find the avulsed tooth and handle it only via the crown.
Gently irrigate it to to clean off debris, but do not scrub the tooth as this may remove tissue that can aid in its survival.

\triplesubsection{Replant the tooth}
In patients with a degraded level of consciousness, there is risk of aspiration of the tooth, and replantation should be avoided.
If the patient is oriented to A+O$\times$4, immediately replant the tooth in its socket.
The incisors and canines are most commonly affected, so use the images in \autoref{fig:tooth_orientation} to correctly orient the tooth.
A replanted tooth can be held in place during transit by having the patient gently bite down on gauze placed over the tooth.

\begin{figure}[htbp]
\caption{Tooth Orientation\supercite{bizhan}}
\label{fig:tooth_orientation}
\centering
\includesvg[width=\textwidth, height=4.5cm, keepaspectratio]{tooth-orientation}

\vspace{0.5cm}
{\footnotesize\centering Orientation of the maxillary left central incisor showing the facial view (left) and lateral view (right).}
\end{figure}

\triplesubsection{Transport the tooth}
If replantation is not possible either due to pain or psychological discomfort, prepare the tooth for transport.
There are commercially available tooth preservation kits\index{tooth preservation kit} that are specifically designed for this, and they are ideal.
Other solutions in order of preference are cow's milk (any fat content), isotonic saline, the patient's own saliva.\supercite[p.267-8]{nols}\textsuperscript{,}\supercite[p.1607]{tintinallis}
Do not use tap water to transport the tooth.
Keep the tooth cool and protected from impact or rubbing during transit.
If the reason for not replanting the tooth is pain alone, wrapping the tooth in a thin layer of gauze and placing it in the patient's mouth against their cheek or between their gum and lip is also an acceptable transportation method and may be ideally in wilderness environments with long transit times.

\index{avulsions!tooth|)}
\index{trauma!dental|)}
\index{trauma!oral|)}

\section*{Summary}

Injuries to the mouth generally heal on their own, but lacerations to the tongue or lip indicate treatment by advanced medical care.
Many dental injuries can lead to permanent tooth loss, but quick treatment drastically increases the chances of the tooth's long-term survival.
Quickly getting a patient to an emergency department or dentist can allow for salvaging of the damaged or lost tooth.
Lost fragments may be reattached by trained medical professionals, so find them if you can.
You can temporarily replant an avulsed tooth at the scene.
