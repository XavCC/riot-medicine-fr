\chapter{Blood Glucose Disorders}
\label{ch:bg_disorders}

\epigraph{
Fascist scum /
Expected a parade /
Your welcoming committee /
Was an antifa brigade}
{G.L.O.S.S., \textit{Fight}\supercite{gloss-trans-day-of-revenge}}

\index{blood glucose!disorders@disorders of|(}

\noindent
For the human body to function and remain healthy, blood glucose levels need to remain within a narrow range.
The human body naturally maintains this range through different hormones, but disease and improper nutrition can cause unhealthily high and low blood glucose.
Eating too little, especially combined with exercising can lead to low blood sugar levels causing fatigue.
Diabetes can cause high blood glucose, and its management can overcorrect and lead to low blood glucose.
Hundreds of millions of individuals worldwide live with diabetes, and medics should be familiar with the disorder in order to assist with treatment.

It is important to remember that when treating patients with diabetes, the patient understands their body and how to manage their blood glucose levels better than you.
They have been living with diabetes for years or perhaps their entire life, and you have just met them.
For patients who are alert and oriented, in most cases, your task as a medic is to remind them to eat or ask if they have taken their medication.

\section*{Physiology}

The physiology behind diabetes and blood glucose disorders is complex.
This section will only focus on the variants that medics are likely to encounter.

\subsection*{Blood Glucose Homeostasis}
\index{homeostasis!blood glucose@of blood glucose|(}
\index{pancreas|(}
\index{insulin|(}
\index{glycogen|(}

Insulin is an anabolic hormone produced by the beta-cells of the pancreatic islets.
Anabolism is the set of metabolic pathways that use energy to build more complex molecules out of smaller units.
The presence of insulin promotes the absorption of of carbohydrates into fat, skeletal muscle, and liver cells.
Absorbed carbohydrates are turned into glycogen (an energy store made of chains of simple sugars) and triglycerides (fat).
As a result of insulin secretion, blood glucose (\acrshort{BG}) levels decrease.

\index{glucagon|(}

Glucagon is a catabolic hormone produced by the alpha-cells of the pancreatic islets.
Catabolism is the set of metabolic pathways that breakdown molecules into smaller units either for energy or as part of anabolic reactions.
The presence of glucagon causes the liver to convert glycogen into glucose.
This increases blood glucose levels.

\index{glycogen|)}

The beta-cells are sensitive to glucose, and as glucose levels rise, insulin is secreted.
The alpha-cells are sensitive to insulin, and as insulin levels rise (and glucose levels drop), glucagon is secreted.
When we eat, our blood glucose levels rise for a few hours, and so do our insulin levels, before slowly dropping.
Together these two types of cells create a negative feedback loop that keeps blood glucose levels within a narrow range.
This is called glucose homeostasis.

\index{glucagon|)}
\index{insulin|)}
\index{pancreas|)}
\index{homeostasis!blood glucose@of blood glucose|)}

% TODO nice-to-have: graph showing time of day vs. BG / insulin (maybe also glucagon)

\subsection*{Diabetes Mellitus}
\index{diabetes mellitus|(}

Diabetes mellitus, commonly called diabetes, is a group of chronic metabolic disorders characterized by hyperglycemia.
The two main types of diabetes are Type 1 and Type 2.
Type 1 diabetes is characterized by an almost complete lack of circulating insulin and a failure of the beta-cells to respond to insulinogenic stimuli, often caused by the destruction of the beta-cells.\supercite[p. 1441]{tintinallis}
Type 2 diabetes is characterized by a relative lack of insulin and failure of target tissues to respond to insulin.\supercite[p. 1445]{tintinallis}

\subsubsection*{Diabetic Ketoacidosis}
\index{ketoacidosis!diabetic|see {diabetic ketoacidosis}}
\index{diabetic ketoacidosis|(}
\index{homeostasis!blood pH@of blood pH|(}

\index{ketosis|(}
The body stores energy as glycogen and triglycerides in adipose tissue.
These stores can be metabolized to make glucose available for use.
When glycogen stores are depleted, the body breaks down fatty acids to make glucose available.
This metabolic state is called ketosis.\footnotemark[1]
When this occurs in healthy humans, it is known as physiological ketosis, and \acrshort{BG} and blood pH remain normal.
In pathological cases, excessive production of ketones can lead to production of acid at a rate greater than can be removed by the kidneys\index{kidneys} (metabolic acidosis\index{metabolic acidosis}).
\index{ketosis|)}

Diabetic ketoacidosis (\acrshort{DKA}) results from an absence of insulin causing the release of fatty acids from adipose tissue where they are broken down in the liver thus releasing ketones into the blood.
The causes the acidification of the blood.
\acrshort{DKA} is more common in individuals with Type 1 diabetes than Type 2, and it is a life-threatening condition.

\index{homeostasis!blood pH@of blood pH|)}

\footnotetext[1]{
This is the basis of Keto (the Ketogenic Diet) and is where the diet gets its name.
}

Some individuals with diabetes use an insulin pump that delivers a steady dose of insulin through out the day.
If their pump fails or is disabled, \acrshort{DKA} can develop within an hour.\supercite[p. 1442]{tintinallis}

\index{diabetic ketoacidosis|)}

\index{diabetes mellitus|)}

\section*{Blood Glucose Management}
\index{blood glucose!management|(}

Individuals with diabetes have to take steps to manage their blood glucose levels.
This includes diet and lifestyle changes, taking medications, and taking insulin injections.
These changes are often guided by use of a blood glucose meter either done by pricking a finger or via continuous monitoring.
Discussions of general blood glucose management and long-term symptoms are outside the scope of this book.

Blood glucose is measured either using molar concentration (millimoles per liter, mmol/L) or mass concentration (milligrams per deciliter, mg/dL).
The two units are related by a factor of 18 so that \SI{1}{\mmol/\liter} of glucose is equivalent to \SI{18}{\mg/\deci\liter}.
The unit used by individuals with diabetes and medical professionals will depend on your region.

\begin{table}[htbp]
\caption{Normal and Target Blood Glucose Levels}
\label{tab:normal_blood_glucose}
\centering
\begin{tabular}[c]{|l|c|c|}
    \hline
    \multicolumn{1}{|c|}{\textbf{Individual}} & \textbf{Fasting \acrshort{BG}} & \textbf{Post-Meal \acrshort{BG}} \\
	\hline
    \multirow{2}{*}{Non-diabetic} & 4.4--\SI{5.5}{\mmol/\liter}     & 4.4--\SI{7.7}{\mmol/\liter}     \\
                                  & (80--\SI{100}{\mg/\deci\liter}) & (80--\SI{140}{\mg/\deci\liter}) \\
    \hline
    \multirow{2}{*}{Diabetic}     & 4.4--\SI{7.2}{\mmol/\liter}     & 4.4--\SI{10.0}{\mmol/\liter}    \\
                                  & (80--\SI{130}{\mg/\deci\liter}) & (80--\SI{180}{\mg/\deci\liter}) \\
    \hline
\end{tabular}
\end{table}

Normal ranges for adults without diabetes and target ranges for adults with diabetes can be found in \autoref{tab:normal_blood_glucose}.
High \acrshort{BG}, often caused by eating too much without taking sufficient insulin, is called \gls{hyperglycemia}.
Low \acrshort{BG}, often caused by eating too little or taking too much insulin, is called \gls{hypoglycemia}.
However, neither condition is limited to patients with diabetes, and both are discussed in more detail later in this chapter.

% TODO include hypo, normal, normal-high, hyper in a single chart

\subsection*{Blood Glucose Testing}

There are two main methods of testing blood glucose: fingerprick testing and continuous monitoring.

Continuous monitoring uses a sensor placed under the skin whose readings are read via a monitoring device or mobile phone.
The glucose level is taken from interstitial fluid rather than blood, so measurements may lag behind blood glucose levels by 5 to 15 minutes.
For this reason, they should not be used as a diagnostic tool during emergencies.

% TODO consider including how to use continuous monitor as secondary means of testing

\begin{figure}[htbp]
\caption{Blood Glucose Measurement\supercite{snailsnail}}
\centering
\begin{subfigure}[b]{4cm}
    \centering
	\includesvg[width=\textwidth, height=5cm, keepaspectratio]{blood-glucose-meter}
	\caption{Meter}
    \label{fig:blood_glucose_meter}
\end{subfigure}
\begin{subfigure}[b]{2.5cm}
    \centering
	\includesvg[width=\textwidth, height=2.5cm, keepaspectratio]{lancet}
    \vspace{1cm} % to get this somewhat vertically centered with the meter
	\caption{Lancet}
    \label{fig:lancet}
\end{subfigure}
\end{figure}

Fingerprick testing involves periodically pricking a finger to draw blood for reading by a glucose meter (\autoref{fig:blood_glucose_meter}).
Fingerpricking is done with a lancet (\autoref{fig:lancet}), either single-use disposable device or reusable lancing device with a disposable lancet tip.
Before pricking a patient's finger, obtain consent to do so.
To measure \acrshort{BG} using a glucose meter, use the following steps:

\begin{enumerate}
    \item Prepare one of the patient's fingers using disinfectant or an alcohol wipe.
    \item Prepare your lancet by removing the safety cap and, if applicable, cocking the lancing mechanism.
    \item Insert a reading strip into the glucose meter and turn it on.
    \item Prick the patient's fingertip with the lancet.
    \item Squeeze their fingertip to force out a drop of blood.
    \item Hold the end of the test strip against the drop of blood.
    \item Read the \acrshort{BG} level from the glucose meter.
\end{enumerate}

% TODO include an illustration showing finger, blood droplet, and monitor

Blood glucose meters are generally accurate to within 15\% of the actual value.
This level of accuracy is sufficient for detecting hyperglycemia, but it may not be sensitive enough for detecting hypoglycemia.
Patients with a low to normal \acrshort{BG} reading can be safely treated as if they have hypoglycemia.

\index{blood glucose!management|)}

\section*{Hypoglycemia}
\index{hypoglycemia|(}

\Gls{hypoglycemia}, colloquially ``low blood sugar,'' is abnormally low levels of blood glucose.
For individuals with diabetes, a \acrshort{BG} of \SI{3.9}{\mmol/\liter} (\SI{70}{\mg/\deci\liter}) is used as a diagnostic level for hypoglycemia.
This number is not absolute, and individuals may have symptoms above this number or may be symptom free below.
A diagnosis of hypoglycemia can be confirmed if hypoglycemic symptoms improve following the administration of glucose.

In patients with diabetes, hypoglycemia may be caused by taking too much insulin or anti-diabetic drugs relative to a food consumed.
Different insulins have different durations of action, but for many the peak action is between 1 to 4 hours after injection.
This is when patients are more susceptible to hypoglycemia.

In general, mild hypoglycemia may be caused by eating too little, prolonged exercise, or alcohol consumption among other causes.

The brain stores a limited amount of glycogen and requires a constant supply of glucose to fuel its high metabolic activity.
A reduction in available glucose via lowered \acrshort{BG} leads to reduced cognitive function, fatigue, and general weakness.
Untreated hypoglycemia can lead to death.

\subsection*{Signs and Symptoms}

Early signs of hypoglycemia include hunger, fatigue, weakness, irritability, anxiety, dizziness, and headaches.
More obvious early signs are sweating and shaking.
Patients may also experience a loss of coordination (\gls{ataxia}), \gls{tachycardia}, pallor, and skin that is cool and clammy.
As hypoglycemia progresses, patients may have more obvious mental changes including confusion and disorientation.
Severe hypoglycemia can result in seizures, coma (commonly called diabetic coma\index{coma!diabetic}\index{diabetic coma}), and death.

Hypoglycemia typically has rapid onset in patients with diabetes and may present with hypothermia\index{hypothermia!hypoglycemia@in hypoglycemia}.\supercite{hypothermia-diabetes}
Patients without diabetes may have a more prolonged onset and less severe symptoms.
This may present as simply fatigue and irritability (the ``hangries''\footnotemark[2]), but it may also include headaches, nausea, and vomiting.

\footnotetext[2]{A portmanteau of ``hungry'' and ``angry.''}

\subsection*{Treatment}

Treatment is to increase the patient's \acrshort{BG} and consider advanced medical care.

\index{dextrose|see {glucose}}
\index{glucose|(}
\triplesubsection{Administer glucose}
This is ideally done by administering glucose (dextrose).
Fructose\index{fructose!hypoglycemia@in hypoglycemia} does not cross the blood-brain barrier, and it should be avoided in favor of other glucose-containing carbohydrates.\supercite[p. 1444]{tintinallis}
Other options for glucose are sugar (sucrose) and simple carbohydrates such as white bread (with a small amount of water to aid digestion).
Protein does not meaningfully increase \acrshort{BG} in hypoglycemic patients.
Diet soft drinks\footnotemark[3] are not effective treatment because although they taste sweet, they have no sugar content.
If patients are alert and oriented, dextrose tablets, glucose gel, or other sugary food or drink can be administered orally.

\footnotetext[3]{
Such as those sweetened with aspartame, saccharine, stevia, or sucralose.
}

\quadruplesubsection{Direct administration}
Patients with reduced levels of consciousness or who are unconscious need to have glucose carefully administered by a medic.\supercite[p. 225]{nols}
Once the patient's airway has been managed, place sugary gels or powders under their tongue, between their gums and cheek, and between their gums and lips.
Only a small amount of powder or gel should be used to reduce the risk of aspiration.
The sugars will be slowly absorbed by the oral mucosa.
After administering, medics should continue to monitor the patient's airway.

\index{glucose|)}

\triplesubsection{Do not remove insulin pump}
If patients have an insulin pump, do not attempt to remove or disable it as this may rapidly lead to hyperglycemia.\supercite[p. 1444]{tintinallis}

% TODO ? glucagon injections

\triplesubsection{Consider evacuation}
Patients without diabetes and minor signs of hypoglycemia (irritability, nausea, vomiting) who recover when treated with food and drink generally do not require evacuation, though they should at least eat a small meal.
Patients who are diabetic with minor alterations to their mental state such as irritability and confusion and who recover when given glucose should be recommend to leave the action with a buddy.
Evacuate to advanced medical care all patients with significant alterations to their mental state including decreased levels of consciousness and patients whose hypoglycemia cannot be explained by diabetes or eating too little.

% TODO don't jump right back into the action because of insufficient dampening of BG levels

\subsection*{Prevention}

Individuals with diabetes typically know how to manage their blood glucose, insulin, or other medications.
Mild hypoglycemia in individuals without diabetes is often caused by insufficient caloric intake, especially during long actions.
During actions that are more fun and celebratory, youthful exuberance and poor planning may lead to insufficient calorie consumption.
For more militant actions, pre-action nervousness and adrenaline during actions may contribute to skipping meals.

Individuals and groups you work with should be encouraged to eat before and during actions to prevent hypoglycemia.
Hypoglycemia may lead to mental and physical fatigue which may lead to other injuries or arrest.
Consider suggesting that people force themselves to eat before actions despite nervousness and a lack of hunger.
However, individuals may have stress and anxiety around food or they may have an eating disorder, so recommendations that individuals eat should be carefully phrased, gentle, and nothing more than explanatory of the reasons why one should eat before or during an action.

\index{medics!self-preservation|(}
\index{self-care|(}
This advice applies to medics themselves.
Skipping meals and not snacking can make it very difficult to render care.
\index{self-care|)}
\index{medics!self-preservation|)}

\index{hypoglycemia|)}

\section*{Hyperglycemia}
\index{hyperglycemia|(}

Medics will typically only see hyperglycemia in patients with diabetes.
\Gls{hyperglycemia} is \acrshort{BG} over \SI{11.1}{\mmol/\liter} (\SI{200}{\mg/\deci\liter}), though symptoms may not present until 13.8 to \SI{16.7}{\mmol/\liter} (250 to \SI{300}{\mg/\deci\liter}).
It may be caused by insufficient insulin or diabetic medication especially after eating.
Hyperglycemia generally develops slowly.
Patients who are ill are at higher risk of hyperglycemia in general, and it may develop within hours in ill patients.

\subsection*{Signs and Symptoms}

Early symptoms of hyperglycemia include nausea, vomiting, increased thirst, and increased volume of urine output.
The increased urine output creates a state of dehydration, and the patient may have flushed, dry skin.
Patients may have headaches, blurred vision, or fatigue.
They may have altered mental states and appear ``drunken.''
Symptoms of severe hyperglycemia include seizures, coma, and death.

\index{diabetic ketoacidosis|(}
Patients may have diabetic ketoacidosis (\acrshort{DKA}) which presents with breath that has a fruity odor or smells of acetone.\footnotemark[4]
A sign of \acrshort{DKA} is shallow, rapid breading.
Severe \acrshort{DKA} presents with deep, labored breathing (Kussmal respiration\index{Kussmal respiration}).
This hyperventilation removes carbon dioxide from the blood which raises the pH to counter the acidosis.
If you or the patient has urine ketone test strips, the patient may want to test themself.
\index{diabetic ketoacidosis|)}

\footnotetext[4]{
For reference, acetone is the primary ingredient in nail polish remover.
}

\subsection*{Treatment}

Hyperglycemic patients should be evacuated to advanced medical care in all cases.\supercite[p. 225]{nols}
Provide supportive care during evacuation including airway management and treatment for shock.
If the patient is alert enough, give them fluids to help prevent dehydration.
Do not administer insulin to the patient,\supercite[p. 225]{nols} but if the patient is alert and oriented, allow them to self administer.
Like with hypoglycemia, if the patient has an insulin pump, do not attempt to remove or disable it.\supercite[p. 1444]{tintinallis}

If you cannot differentiate between a patient who is hypoglycemic or hyperglycemic, you may still administer glucose as extra glucose will not harm hyperglycemic patients.

\index{hyperglycemia|)}

\section*{Summary}

Blood glucose related disorders may be most often found in patients with diabetes.
Hypoglycemia may result from excess insulin or insufficient food consumption, treatment is giving the patient glucose and possible evacuation.
Hyperglycemia may result from insufficient insulin in diabetic patients, and treatment is preventing dehydration and evacuation.
When the two cannot be differentiated, glucose should be administered.

\index{blood glucose!disorders@disorders of|)}
