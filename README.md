# Riot Medicine

*Riot Medecine* est un livre placé dans le docmain public et conçu pour

*Riot Medicine* is a public domain book designed to cover the basics of medicine in insurrectionary environments.

**Ce dépot est créé et ouvert dans l'intention d'en produire une version traduite en langue française, avec dicussion d'origine avec l'autaire (voir [ici](https://gitlab.com/hakan-geijer/riot-medicine-en/-/issues/241))**

## Table des contenus

+ En Français
 + tbd
+ English
  + [Contributing](#contributing)
    + [Building](#building)

## Contributing

### Building

Running `make pdf` should generate a document. Errors will be written to the log file.

### Development Environment

*Riot Medicine* is written using `LaTeX`. You will need to install this on your system.
Additionally, you will need the following packages in addition to the base packages that are often installed with distributions of `LaTeX`:

- `svg` version 2.0 or above

Do this with `tlmgr install`.

### LaTeX Style Guide

- One sentence per line (don't wrap)
- Labels use the following convention: `label_type:item_name`
    - `pt` for parts
    - `ch` for chapters
    - `sec` for sections
    - `tab` for tables
    - `fig` for figures
    - `lst` for lists

## License

*Riot Medicine* and all files, documents, and source code contained in this repository is available under the Creative Commons Zero 1.0 Universal license.
Effectively, this project is public domain.
Note that some illustrations are not public domain, and those can be found in [`content/copyright.tex`](./content/copyright.tex).
