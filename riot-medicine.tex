\documentclass[a5paper,twoside,openany]{book}

\usepackage[utf8]{inputenc}

% need to import before other packages to prevent options clash
\usepackage[hidelinks]{hyperref}

\usepackage[
    alldates=edtf, % ISO-8601 dates in bibliography
    sorting=none,
    style=numeric,
    citestyle=numeric-comp,
]{biblatex}
\usepackage{bookmark} % to get \bookmarksetup
\usepackage{csquotes}
\usepackage{enumitem} % to remove padding on items in lists
\usepackage{epigraph}
\usepackage[
    a5paper,
    total={110mm,170mm},
    left=20mm,
    top=20mm,
]{geometry}
\usepackage[acronym,nopostdot,toc]{glossaries}
\usepackage{graphicx}
\usepackage{imakeidx}
\usepackage[
    columns=2,
    columnsep=20pt,
    indentunit=10pt,
    font=footnotesize,
    totoc=true,
]{idxlayout} % to decrease the font size
\usepackage[xcolor]{mdframed}
\usepackage{microtype} % for kerning/spacing/hyphenation
\usepackage{multirow}
\usepackage{numprint} % for \npthousandsep
\usepackage{pgfplots} % for charts
\usepackage{siunitx}
\usepackage{subcaption}
\usepackage{svg}
\usepackage{tabularx}
\usepackage{titlesec} % for resizing the spacing on sections
\usepackage{titleps}
\usepackage{titling}
\usepackage{tikz}
\usepackage{verbatimbox} % for \addvbuffer
\usepackage{xifthen} % for \ifthenelse and \isempty

\input{hyphenation} % hyphenations for words not in the standard dictionary

\hypersetup{breaklinks=true}

\addbibresource{references.bib}
\renewcommand*{\bibfont}{\footnotesize} % decrease the size of the text in the bibliography

\renewcommand{\contentsname}{Table of Contents} % Rename the TOC
\renewcommand{\thefootnote}{\roman{footnote}} % set footnotes to use Roman numerals

\setlength{\epigraphwidth}{0.5\textwidth} % make quotes at start of chapters wider

\graphicspath{{./images/binary/}} % set search path for binary images
\svgpath{{./images/svg}} % set search path for SVG images

\setlist{noitemsep} % remove vertical space between items in itemize/enumerate

\npthousandsep{,} % for displaying large numbers

% command for subtitle on title page
\newcommand{\subtitle}[1]{
    \posttitle{
        \par\end{center}
        \begin{center}\large#1\end{center}
        \vskip0.5em
    }
}

% TODO this could be prettier, but it saves pages for now
\titleformat{\chapter}[hang]{\huge\bfseries}{\thechapter\ :\ }{0.ex}{\huge\bfseries\raggedright}
\titlespacing*{\chapter}{0ex}{1ex}{2ex plus 1ex minus 0.5ex}
\titlespacing*{\section}{0ex}{2.5ex plus 0.5ex minus 0.2ex}{.2ex plus 0.2ex minus 0.1ex}
\titlespacing*{\subsection}{0ex}{1.5ex plus 0.5ex minus 0.2ex}{.2ex plus 0.2ex minus 0.1ex}
\titlespacing*{\subsubsection}{0ex}{1ex plus 0.5ex minus 0.2ex}{.2ex plus 0.2ex minus 0.1ex}

\newcommand{\triplesubsection}[1]{\textbf{#1.} }
\newcommand{\quadruplesubsection}[1]{\underline{\textit{#1.}} }

% capitalize the autorefs
\renewcommand{\partautorefname}{Part}
\renewcommand{\chapterautorefname}{Chapter}
\renewcommand{\tableautorefname}{Table}
\renewcommand{\figureautorefname}{Figure}

% TODO this can overflow into the margins
\newcommand{\fullref}[1]{\hyperref[{#1}]{\autoref*{#1} (``\nameref*{#1}'')}}

% environment for "war stories"
% TODO remove left indentation on title
% TODO make author a subtitle?
\newenvironment{war-story}[2]
{
    \begin{mdframed}[
        frametitle={
            \ifthenelse{\isempty{#1}}{}{#1\ifthenelse{\isempty{#2}}{}{ - #2}}
        },
        backgroundcolor=lightgray!10,
        skipabove=0.7cm,
        skipbelow=0.7cm,
        innertopmargin=0.3cm,
        innerbottommargin=0.3cm,
    ]
}
{\end{mdframed}}

\newpagestyle{main}{
  \sethead[\thepage\hskip 1em\textit{Riot Medicine}]% odd-left
          []% odd-center
          []% odd-right
          {}% even-left
          {}% even-center
          {\textit{\chaptertitle}\hskip 1em\thepage}% even-right
}

\makeindex[
    columns=2,
    columnsep=15pt,
    columnseprule,
    intoc=true,
]

\glsdisablehyper % disable hyperrefs for glossary terms because they are visual clutter
\makeglossaries
\input{acronyms}
\input{glossary}

\title{Riot Medicine}
\subtitle{First Edition, First Revision\IfFileExists{git-commit}{ (\input{git-commit}\unskip)}{}}
\author{Håkan Geijer}
\date{2023-03-06}

\begin{document}
\pagestyle{main}

\inputencoding{utf8}
\renewcommand{\bibname}{References}

\frontmatter
% TODO make the title bigger, prettier, add logo
\maketitle

\include{src/copyright}
\include{src/disclaimer}
\include{src/acknowledgements}
\input{src/dedication}

% start the header styling here to prevent weird styling in the front matter pages
\cleardoublepage % have to double clear because latex is somehow stupid and otherwise left/right and even/odd get messed up

\setcounter{tocdepth}{0} % only show parts and chapters (not sections, subsections, etc.)
\tableofcontents

\include{src/preface}
\include{src/intro}

\mainmatter

\part{Organize!}
\include{src/organize/responsibilities}
\include{src/organize/organizational-structures}
\include{src/organize/pre-action}
\include{src/organize/post-action}
\include{src/organize/training}

\part{Medicine}
\include{src/medicine/patient-assessment}
\include{src/medicine/patient-evacuation}
\include{src/medicine/psychological-care}
\include{src/medicine/medication}
\include{src/medicine/alternative-medicine}
\include{src/medicine/basic-life-support}
\include{src/medicine/wound-management}
\include{src/medicine/riot-control-agent-contamination}
\include{src/medicine/shock}
\include{src/medicine/brain-and-spinal-cord-injuries}
\include{src/medicine/chest-injuries}
\include{src/medicine/fractures-and-dislocations}
\include{src/medicine/athletic-injuries}
\include{src/medicine/burns}
\include{src/medicine/combat-injuries}
\include{src/medicine/oral-and-dental-injuries}
\include{src/medicine/cold-injuries}
\include{src/medicine/heat-illness}
\include{src/medicine/respiratory-and-cardiac}
\include{src/medicine/allergies-and-anaphylaxis}
\include{src/medicine/blood-glucose-disorders}
\include{src/medicine/drug-overdoses}
\include{src/medicine/unresponsive-states}

\part{Equipment}
\include{src/equipment/medical-supplies}
\include{src/equipment/ppe}
\include{src/equipment/other-gear}
\include{src/equipment/packing}

\part{Tactics}
\label{pt:tactics}
\include{src/tactics/general-tactics}
\include{src/tactics/action-tactics}
\include{src/tactics/opsec}
\include{src/tactics/radio-operation}

\backmatter
\bookmarksetup{startatroot} % to prevent the afterword (and following chapters) from being part of the previous Part

\include{src/afterword}
\include{src/further-reading}

\printglossary[type=\acronymtype,nonumberlist]
\printglossary[nonumberlist]

\clearpage
\printindex

\clearpage
\phantomsection\addcontentsline{toc}{chapter}{\bibname} % add the references to the TOC
\defbibnote{bib-prenote}{Remember, folx: paying for knowledge is bullshit.
Pirate papers from \url{https://sci-hub.tw/}.
Download books from \url{https://libgen.lc/}.
Information wants to be free.
}
\printbibliography[prenote=bib-prenote]

\end{document}
